# Syntax

The different todo item types

    - [ space ]     = A todo-item that is not yet started
    - [ w ]         = Waiting answer/feedback
    - [ m ]         = Item moved to newer todo-list (deprecated)?
    - [ n ]         = Not to be done. Ever.
    - [ h ]         = On hold for now.
    - [ x ]         = Done! Be happy and celebrate.
    - [ space ] X.  = X equals prioritation where 1 is high 5 is low

Different tags

    - [space] 1. Take leadership over the world @worlddomination
    - [space] 2. Crush resistance @worlddomination
    - [space] 3. Celebrate world domination @worlddomination
    - [space] Send birthdaycard to Mom @housekeeping

Tags
----

- Tags are used to give context to different todos `@home`

Filters
-------

Filters are used to target only certain `.md` files.

- `*binarybrick` would only search `*binarybrick.md`
- `binarybrick*` would only search `binarybrick*.md`
- `*binarybrick*` would only search `*binarybrick*.md`

## Search examples

`php ~/Dropbox/Projects/Console\ Todo/todo-html.php @high @now @critical @panic`

Searches `*.md` files for tags `@high @now @critical @panic`

`php ~/Dropbox/Projects/Console\ Todo/todo-html.php @panic *binary`

Searches `*binary.md` files for tags `@panic`

## Config

Use `config.inc.php.example` as base config

### Regexp Examples

- `(\*[a-z]+\*|\*[a-z]+|[a-z]+\*)`

    - [ ] Download models @cnc @furk
    - [ ] Download raw data @cnc *file
    - [ ] Update model @cnc *file*
    - [ ] Re-download models file*
    - [ ] Re-download models file*
    - [ ] *file refactor model @cnc
    - [ ] file* refactor model @cnc
    - [ ] *file* refactor model @cnc





