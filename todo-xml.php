<?php

/**
 * Console Todo 3.0
 * Author: Joakim Vindgard
 * 
 * -----------------------------------------
 * 
 * This is an atempt to do it right for once
 */




        not working!








// Get base class
include('todo.class.php');
include('config.inc.php');

// Run todo-class and send arguments
$todo = new ToDo($argv, $config);
$todo->search();

// Get results
$result = $todo->todos;

# Build XML
$xml = '<items>';
    if(count($todo->todos) < 1){
        $xml .= '
          <item arg="" valid="NO" autocomplete="" type="file">
            <title>No match for: '.$todo->query.'</title>
            <subtitle>Please try again...</subtitle>
            <icon type="fileicon"></icon>
          </item>
          ';
    }else{
      foreach ($todo->todos as $id => $todo) {
        $xml .= '
          <item arg="~/Dropbox/'.$todo['filePath'].'" valid="YES" autocomplete="'.$todo['fileName'].'" type="file">
            <title>'.htmlspecialchars($todo['todo']).'</title>
            <subtitle>'. $todo['fileName'].'</subtitle>
            <icon type="fileicon"></icon>
          </item>
          ';
      }
    }
$xml .= '</items>';

echo $xml;