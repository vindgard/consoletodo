<?php

/**
 * Console Todo Class 1.0
 * Author: Joakim Vindgard
 * Date: 2015-09-07
 */

class Todo
{
    private array $config = [
        'fileEnding' => 'md',
        'fileNameFilter' => '*',
        'excludeFiles' => [],
        'excludeDirs' => [],
        'tags' => []
    ];

    private array $tagFilters = [];
    private string $tagEreg = '/\s#([\wåäöÅÄÖ\/]+)/';
    #private string $tagEreg = '/\s(@.+?)\b/';
    // new tag ereg `(#[\wåäöÅÄÖ/]+)\b` needs testing
    private array $todos = [
        'todo' => [],
        'done' => []
    ];

    private bool $debug = false;

    public function __construct()
    {
    }

    public function debug(): void {
        $this->debug = true;
    }

    public function setPath(string $path): void {
        $this->config['path'] = $path;
    }

    private function getPath() {
        return $this->config['path'];
    }

    public function setFileEnding(string $fileEnding): void {
        $this->config['fileEnding'] = $fileEnding;
    }

    public function setTag(string $tag): void {
        $this->config['tags'][] = $tag;
    }

    public function addExcludeFile(string $file): void {
        $this->config['excludeFiles'][] = $file;
    }

    public function addExcludeDir(string $dir): void {
       $this->config['excludeDirs'][] = $dir;
    }

    private function getFileEnding() {
        return $this->config['fileEnding'];
    }

    public function setFileNameFilter(string $fileNamePart): void {
        $this->config['fileNameFilter'] = $fileNamePart;
    }

    private function getFileNameFilter(){
        return $this->config['fileNameFilter'];
    }

    private function getTagFilter(): ?array {
        return $this->config['tags'];
    }

    private function getExcludeFiles(): array
    {
        return $this->config['excludeFiles'];
    }

    private function getExcludeDirs(): array
    {
        return $this->config['excludeDirs'];
    }

    private function buildGrepQuery(): string
    {
        // Jump into specified path
        $grepQuery = 'cd ' . $this->getPath() . ';';
        $grepQuery .= 'grep';

        // This must be before the excludes
        $grepQuery .= ' --include="' . $this->getFileNameFilter() . '.' . $this->getFileEnding() . '"';

        // Add exclude dirs
        foreach ($this->getExcludeDirs() as $dir) {
            $grepQuery .= ' --exclude-dir="' . $dir . '"';
        }

        // Add exclude files
        foreach ($this->getExcludeFiles() as $file) {
            $grepQuery .= ' --exclude="' . $file . '"';
        }

        // Look for "- [ ]" and "- [x]"
        $grepQuery .= ' -Rne "\- \[[x\ ]\]" . ';

        // Search for tags
        if (null !== $this->getTagFilter() && count($this->getTagFilter()) > 0) {
            $grepQuery .= '|grep "' . implode("\|", $this->getTagFilter()) . '"';
        }

        $this->debugOutput($grepQuery);

        // Ignore @done tags
        //$this->grepQuery .= '|grep -v "@done"';
        return $grepQuery;
    }

    private function extractTodos($raw_result)
    {
        if (null === $raw_result) {
            return;
        }

        // Get rows by exploding on line beak
        $rows = explode("\n", $raw_result);

        // Iterate rows
        foreach ($rows as $todo_row) {
            // If todo row is empty, ignore it
            if ($todo_row === '') {
                continue;
            }

            // Remove tabs
            $todo = preg_replace('/\t+/', '', $todo_row);

            // Get todo status
            $todo_status = preg_match_all("-\[x\]-", $todo_row) ? 'done' : 'todo';

            // Could not explode on : since it broke links and stuff
            $todo_row = explode(
                '[---]',
                str_replace(
                    ['- [ ]', '- [x]'],
                    '[---]',
                    $todo_row
                )
            );

            $parts = explode(":", $todo_row[0]);
            // 0 = filename
            // 1 = line number

            $raw_description = trim($todo_row[1]);

            // If actual todo item is empty, bail
            if ($raw_description === '') {
                continue;
            }

            $raw_file_name = $parts[0];
            $raw_file_row_num = $parts[1];

            unset($parts);

            // Tags
            preg_match_all($this->tagEreg, $raw_description, $tags);
            $raw_description = preg_replace($this->tagEreg, '', $raw_description);

            // Get todo priority
            preg_match("/^[0-9]{1}(?=[.\\ ])/usm", $raw_description, $priority);

            // Create array with prio, file and todo. Strip "- " syntax form the todo
            $todo = [
                'priority' => (count($priority) > 0 ? $priority[0] : 9),
                'filePath' => $raw_file_name,
                'fileName' => basename($raw_file_name),
                'fileNameShort' => str_replace('.' . $this->getFileEnding(), '', basename($raw_file_name)),
                'lineNumber' => $raw_file_row_num,
                'tags' => $tags[1],
                'todo' => $raw_description
            ];

            $this->todos[$todo_status][] = $todo;
        }

        // Sort data
        array_multisort($this->todos);
    }

    public function search()
    {
        // Create array from raw GREP text todos
        $this->extractTodos(shell_exec($this->buildGrepQuery()));
    }

    public function outputTodosRaw(){
        die(print_r($this->todos));
    }

    public function getNumTodos()
    {
        return count($this->todos['todo']);
    }

    public function getNumTodosDone()
    {
        return count($this->todos['done']);
    }

    public function getTodos(): ?array
    {
        return $this->todos['todo'];
    }

    public function getTodosGroupedByFilenames($includeDone = false): array
    {
        $todo_files = [];
        $status = [];

        if (array_key_exists('todo', $this->todos)) {
            $status['todo'] = $this->todos['todo'];
        }

        if ($includeDone && array_key_exists('done', $this->todos)) {
            $status['done'] = $this->todos['done'];
        }

        foreach ($status as $todo_status => $todos) {
            foreach ($todos as $todo) {
                $todo_files[$todo['fileName']][$todo_status][] = $todo;
            }
        }

        return $todo_files;
    }

    public function getTodosGroupedByTags($includeDone = false): array
    {
        $todo_tags = [];

        $status = [
            'todo' => $this->todos['todo']
        ];

        if ($includeDone) {
            $status['done'] = $this->todos['done'];
        }

        foreach ($status as $todo_status => $todos) {
            foreach ($todos as $todo) {
                if (count($todo['tags']) === 0) {
                    $todo_tags['untagged'][$todo_status][] = $todo;
                    continue;
                }

                foreach ($todo['tags'] as $tag) {
                    $todo_tags[$tag][$todo_status][] = $todo;
                }
            }
        }
        return $todo_tags;
    }

    private function debugOutput(mixed $output): void {
        if (!$this->debug) {
            return;
        }

        if (gettype($output) === 'string') {
            echo $output . PHP_EOL;
        }

        if (gettype($output) === 'array') {
            print_r($output);
        }
    }

}
