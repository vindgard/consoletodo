<?php

/**
 * Console Todo 3.0
 * Author: Joakim Vindgard
 * Date: 2016-03-30
 * 
 * -----------------------------------------
 *
 * This is an atempt to do it right for once
 */

// Get base class
include('todo.class.php');
include('config.inc.php');

// Run todo-class and send arguments
$todo = new ToDo();
$todo->setPath('/Users/vindgard/syncthing/obsidian-notes');
#$todo->setFileNameFilter('*-05');
$todo->search();

// Get results
$result = $todo->getTodos();

// Dump data
foreach ($result as $key => $value) {
    echo '(' . $value['fileName'] . ') ' . $value['todo'] . (count($value['tags']) > 0 ? ' (' . implode(",", $value['tags']) . ')' : '') . PHP_EOL;
}
